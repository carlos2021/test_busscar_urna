from django.shortcuts import render


from django.http import HttpResponse

def image_upload_view(request):
    """Process images uploaded by users"""
    if request.method == 'POST':
        return HttpResponse(
            {"muchas gracias por usar nuestra herramienta": "hasta la proxima",}
            )
    else:
        return render(request, 'index.html', {"contexto": ""})